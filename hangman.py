# Problem Set 2, hangman.py
# Name: 
# Collaborators:
# Time spent:

# Hangman Game
# -----------------------------------
# Helper code
# You don't need to understand this helper code,
# but you will have to know how to use the functions
# (so be sure to read the docstrings!)
import array
import random
import string

WORDLIST_FILENAME = "words.txt"

def load_words():
    """
    Returns a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r')
    # line: string
    line = inFile.readline()
    # wordlist: list of strings
    wordlist = line.split()
    print("  ", len(wordlist), "words loaded.")
    return wordlist



def choose_word(wordlist):
    secret_word = random.choice(wordlist)

    return secret_word

# end of helper code
# -----------------------------------

wordlist = load_words()
secret_word = choose_word(wordlist)



def is_word_guessed(secret_word, letters_guessed):
    '''
    secret_word: string, the word the user is guessing; assumes all letters are
      lowercase
    letters_guessed: list (of letters), which letters have been guessed so far;
      assumes that all letters are lowercase
    returns: boolean, True if all the letters of secret_word are in letters_guessed;
      False otherwise
    '''
    win = 0
    if letters_guessed == secret_word:
        win = 1
    return win




def get_guessed_word(secret_word, letters_guessed):
    '''
    secret_word: string, the word the user is guessing
    letters_guessed: list (of letters), which letters have been guessed so far
    returns: string, comprised of letters, underscores (_), and spaces that represents
      which letters in secret_word have been guessed so far.
    '''
    string = ""
    for char in secret_word:
        letter_not_guessed = 1
        for letter in letters_guessed:
            if letter == char:
                string += letter
                letter_not_guessed = 0
                break
        if letter_not_guessed == 1:
            string += '_ '
    letters_guessed = string
    return letters_guessed





def get_available_letters(used_letters):
    '''
    letters_guessed: list (of letters), which letters have been guessed so far
    returns: string (of letters), comprised of letters that represents which letters have not
      yet been guessed.
    '''
    string = ''
    abc = 'abcdefghigklmnopqrstuvwxyz'
    if used_letters != '':
        for symbol in abc:
            i = 0
            for char in used_letters:
                i += 1
                if char == symbol:
                    break
                elif i == len(used_letters):
                    string += symbol
        abc = string
    return abc




    
    

def hangman(secret_word):
    '''
    secret_word: string, the secret word to guess.
    
    Starts up an interactive game of Hangman.
    
    * At the start of the game, let the user know how many 
      letters the secret_word contains and how many guesses s/he starts with.
      
    * The user should start with 6 guesses

    * Before each round, you should display to the user how many guesses
      s/he has left and the letters that the user has not yet guessed.
    
    * Ask the user to supply one guess per round. Remember to make
      sure that the user puts in a letter!
    
    * The user should receive feedback immediately after each guess 
      about whether their guess appears in the computer's word.

    * After each guess, you should display to the user the 
      partially guessed word so far.
    
    Follows the other limitations detailed in the problem write-up.
    '''
    vowels = 'aeiou'
    guesses = 6
    warnings = 4
    score = 0
    win = 0
    letters_guessed = "0"
    while guesses > 0:
        letters_guessed = get_guessed_word(secret_word, letters_guessed)
        win = is_word_guessed(secret_word, letters_guessed)
        if win == 1:
            break
        print(letters_guessed)
        guess = input(f"Please guess letter:")
        letter_check = guess[0].lower()
        if letter_check.islower():
            letter_not_guessed = 1
            for letters in letters_guessed:
                if letter_check == letters:
                    if warnings > 0:
                        warnings -= 1
                    else:
                        guesses -= 1
                    print(f"Oops! You've already guessed that letter. You have {warnings} warnings left:")
                    break
            for char in secret_word:
                if letter_check == char:
                    letters_guessed += char
                    letter_not_guessed = 0
                    break
            if letter_not_guessed == 1:
                for vowel in vowels:
                    if letter_check == vowel:
                        guesses -= 2
                        break
                    elif vowel == 'u':
                        guesses -= 1
                print(f"Oops! This letter is not in my word.")
    if guesses == 0:
        print(f"Sorry, you ran out of guesses. The word was {secret_word}")
        pass
    if win == 1:
        print(f"Congratulations, you won!\nYour total score for this game is: {score}")
        pass






# When you've completed your hangman function, scroll down to the bottom
# of the file and uncomment the first two lines to test
#(hint: you might want to pick your own
# secret_word while you're doing your own testing)




# -----------------------------------



def match_with_gaps(my_word, other_word):
    '''
    my_word: string with _ characters, current guess of secret word
    other_word: string, regular English word
    returns: boolean, True if all the actual letters of my_word match the 
        corresponding letters of other_word, or the letter is the special symbol
        _ , and my_word and other_word are of the same length;
        False otherwise: 
    '''
    string = ''
    chars = ''
    i = 0
    f = 1
    for char in my_word:
        if char != ' ':
            if char != '_':
                chars += char
            string += char
    my_word = string
    for char in my_word:
        if char != other_word[i] and char != '_':
            f = 0
        elif char == '_':
            for symbol in chars:
                if symbol == other_word[i]:
                    f = 0
        i += 1
    if f == 1:
        return True
    else:
        return False




def show_possible_matches(my_word, used_letters):
    '''
    my_word: string with _ characters, current guess of secret word
    returns: nothing, but should print out every word in wordlist that matches my_word
             Keep in mind that in hangman when a letter is guessed, all the positions
             at which that letter occurs in the secret word are revealed.
             Therefore, the hidden letter(_ ) cannot be one of the letters in the word
             that has already been revealed.
    '''
    print('Possible word matches are:')
    words = ''
    string =''
    for char in my_word:
        if char != ' ':
            string += char
    my_word = string
    l = len(my_word)
    for word in wordlist:
        res = len(word) == l
        if res:
            for i in range (0, l):
                if my_word[i] != '_':
                    res = not (my_word[i] != word[i])
                else:
                    res = used_letters.find(word[i]) < 0
                if not res:
                    break
            if res:
                words += word + ' '
    return(words)




def hangman_with_hints(secret_word):
    '''
    secret_word: string, the secret word to guess.
    
    Starts up an interactive game of Hangman.
    
    * At the start of the game, let the user know how many 
      letters the secret_word contains and how many guesses s/he starts with.
      
    * The user should start with 6 guesses
    
    * Before each round, you should display to the user how many guesses
      s/he has left and the letters that the user has not yet guessed.
    
    * Ask the user to supply one guess per round. Make sure to check that the user guesses a letter
      
    * The user should receive feedback immediately after each guess 
      about whether their guess appears in the computer's word.

    * After each guess, you should display to the user the 
      partially guessed word so far.
      
    * If the guess is the symbol *, print out all words in wordlist that
      matches the current guessed word. 
    
    Follows the other limitations detailed in the problem write-up.
    '''
    vowels = 'aeiou'
    used_letters = ''
    available_hint = 1
    guesses = 6
    warnings = 3
    score = 0
    win = 0
    letters_guessed = ""
    print(f"Welcome to the game Hangman!\nI am thinking of a word that is {len(secret_word)} letters long.\nYou have 3 warnings left.")
    while guesses > 0:
        abc = get_available_letters(used_letters)
        letters_guessed = get_guessed_word(secret_word, letters_guessed)
        print(f"------------")
        win = is_word_guessed(secret_word, letters_guessed)
        if win == 1:
            i = 0
            string = ' '
            for char in secret_word:
                j = 0
                if i == 0:
                    string = char
                    i = 1
                else:
                    for letter in string:
                        j += 1
                        if letter == char:
                            break
                        elif j == len(string):
                            string += char
            score = len(string)*guesses
            break
        print(f"You have {guesses} guesses left.\nAvailable letters:{abc}")
        guess = input(f"Please guess letter:").lower()
        letter_check = guess[0]
        if letter_check.islower():
            letter_not_guessed = 1
            for letters in used_letters:
                if letter_check == letters:
                    letter_not_guessed = 0
                    if warnings > 0:
                        warnings -= 1
                        print(f"Oops! You've already guessed that letter. You have {warnings} warnings left: {get_guessed_word(secret_word, letters_guessed)}")
                    else:
                        guesses -= 1
                        print(f"Oops! You've already guessed that letter. You have no warnings left, so you lose one guess:{get_guessed_word(secret_word, letters_guessed)}")
                    break
            used_letters += letter_check
            for char in secret_word:
                if letter_check == char:
                    letters_guessed += char
                    letter_not_guessed = 0
                    break
            if letter_not_guessed == 1:
                for vowel in vowels:
                    if letter_check == vowel:
                        guesses -= 2
                        break
                    elif vowel == 'u':
                        guesses -= 1
                print(f"Oops! This letter is not in my word: {get_guessed_word(secret_word, letters_guessed)}")
            else:
                print(f"Good guess:{get_guessed_word(secret_word, letters_guessed)}")
        elif letter_check == '*':
            if available_hint == 1:
                words = show_possible_matches(letters_guessed, used_letters)
                print(words)
                available_hint = 0
            else:
                print('You can not use hint twice!')
        else:
            if warnings > 0:
                warnings -= 1
                print(f"Oops! That is not a valid letter. You have {warnings} warnings left:{get_guessed_word(secret_word, letters_guessed)}")
            else:
                guesses -= 1
                print(f"Oops! That is not a valid letter. You have no warnings left, so you lose one guess:{get_guessed_word(secret_word, letters_guessed)}")


    if guesses == 0:
        print(f"Sorry, you ran out of guesses. The word was {secret_word}.")
        pass
    elif win == 1:
        print(f"Congratulations, you won!\nYour total score for this game is: {score}")
        pass



# When you've completed your hangman_with_hint function, comment the two similar
# lines above that were used to run the hangman function, and then uncomment
# these two lines and run this file to test!
# Hint: You might want to pick your own secret_word while you're testing.


if __name__ == "__main__":
    # pass

    # To test part 2, comment out the pass line above and
    # uncomment the following two lines.
    
    #secret_word = choose_word(wordlist)


###############
    
    # To test part 3 re-comment out the above lines and 
    # uncomment the following two lines. 
    #print(match_with_gaps("a_ ple", "apple"))

    secret_word = choose_word(wordlist)
    hangman_with_hints(secret_word)
    #hangman(secret_word)
